<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function(){

    Route::resource('users', 'UsersController');
    Route::get('users/{id}/destroy', [
        'uses'  => 'UsersController@destroy',
        'as'    => 'admin.users.destroy'
    ]);

    Route::resource('categories', 'CategoriesController');
    Route::get('categories/{id}/destroy', [
        'uses' => 'CategoriesController@destroy',
        'as' => 'admin.categories.destroy'
    ]);

    Route::resource('tags', 'TagsController');
    Route::get('tags/{id}/destroy', [
        'uses' => 'TagsController@destroy',
        'as' => 'admin.tags.destroy'
    ]);

    Route::resource('articles', 'ArticleController');

    Route::resource('donation', 'DonationController');

    Route::resource('donationcategories', 'DonationCategoryController');

    Route::resource('donor', 'DonorController');

    Route::resource('control', 'ControlController');
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('/');

// Route::get('admin/auth/login', [
//     'uses'  => 'HomeController@getLogin',
//     'as'    => 'admin.auth.login'
// ]);

// Route::post('admin/auth/login', [
//     'uses'  => 'HomeController@postLogin',
//     'as'    => 'admin.auth.login'
// ]);

// Route::get('admin/auth/logout', [
//     'uses'  => 'HomeController@getLogout',
//     'as'    => 'admin.auth.logout'
// ]);

