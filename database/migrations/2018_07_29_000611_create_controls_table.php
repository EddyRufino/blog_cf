<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateControlsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('controls', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_master_id')->unsigned()->nullable();
            $table->integer('donation_id')->unsigned()->nullable();
            $table->integer('article_id')->unsigned()->nullable();
            $table->string('estado', 10)->default('1');
            $table->datetime('fecha_salida')->nullable();

            $table->foreign('user_master_id')->references('id')->on('users')
                        ->onDelete('cascade');
            $table->foreign('donation_id')->references('id')->on('donations')
                        ->onDelete('cascade');
            $table->foreign('article_id')->references('id')->on                       ('articles')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('controls');
    }
}
