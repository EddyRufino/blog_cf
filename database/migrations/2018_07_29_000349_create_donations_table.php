<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDonationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 120);
            $table->double('cantidad', 8,2);
            $table->string('estado', 10)->default('1');
            $table->datetime('fecha_entrada')->nullable();
            $table->string('num_serie', 60)->nullable();
            $table->string('unidad_medida', 50)->nullable();
            $table->integer('donationcategory_id')->unsigned();
            $table->integer('donor_id')->unsigned();

            $table->foreign('donationcategory_id')->references('id')->on('donationcategories')
                    ->onDelete('cascade');
            $table->foreign('donor_id')->references('id')->on('donors')
                    ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donations');
    }
}
