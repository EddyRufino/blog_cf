<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * LA TABLA "TAGS" TRABAJA COMO LA TABLA "NECESIDADES"
         */
        Schema::create('tags', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->unique;
            $table->timestamps();
        });

        /**
         * LA TABLA "ARTICLE_TAG" TRABAJA COMO LA TABLA PIVOT DE *"ZONA-AFFECTADA_NECESIDADES"
         */

        Schema::create('article_tag', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('article_id')->unsigned();//ZONA AFECTADA
            $table->integer('tag_id')->unsigned();//NESECIDADES
            //$table->integer('donation_id')->unsigned()->nullable();

            $table->foreign('article_id')->references('id')->on('articles')->onDelete('cascade');
            $table->foreign('tag_id')->references('id')->on('tags')->onDelete('cascade');
            //$table->foreign('donation_id')->references('id')->on('donation');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //Debes tener en cuenta el orden de como vas a eliminar las tablas
        Schema::dropIfExists('article_tag');
        Schema::dropIfExists('tags');
    }
}
