<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /**
         * LA TABLA "ARTICULOS" TRABAJA COMO LA TABLA "ZONA-AFECTADA"
         */
        Schema::create('articles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');//direccion
            $table->string('content')->nullable();
            $table->integer('user_id')->unsigned();
            $table->integer('category_id')->unsigned();//ciudad
            $table->string('slug')->nullable();
            $table->string('estado', 10)->default('1');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');

            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('articles');
    }
}
