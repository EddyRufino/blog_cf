<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Article extends Model //implements SluggableInterface
{
    //use SluggableTrait;
    use sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    protected $table = 'articles';

    protected $fillable = [
        'title',
        'content',
        'category_id',
        'user_id'
    ];

    public function articles()
    {
        return $this->hasMany('App\Article');
    }

    public function users()
    {
        return $this->belongsTo('App\User');
    }

    public function images()
    {
        return $this->hasMany('App\Image');
    }

    public function tags()
    {
        return $this->belongstoMany('App\Tag');
    }

    public function controls()
    {
        return $this->hasMany('App\Control');
    }

}
