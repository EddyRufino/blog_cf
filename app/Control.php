<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Control extends Model
{
    protected $table = 'controls';

    protected $fillable = ['user_master_id', 'donation_id', 'article_id', 'estado', 'fecha_salida'];

    public function article() 
    {
        return $this->belongsTo('App\Article');
    }

    public function donation()
    {
        return $this->belongsTo('App\Donation');
    }

    public function scopeSearch($query, $name)
    {
        return $query->where('name', 'LIKE', "%$name%");
    }
}
