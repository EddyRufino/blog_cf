<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    protected $table = 'donations';

    protected $fillable = [
        'name',
        'cantidad',
        'estado',
        'fecha_entrada',
        'num_serie',
        'unidad_medida',
        'donationcategory_id',
        'donor_id'
    ];

    public function control()
    {
        return $this->hasMany('App\Control');
    }
}
