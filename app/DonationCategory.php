<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DonationCategory extends Model
{
    protected $table = 'donationcategories';

    protected $fillable = ['name'];
}
