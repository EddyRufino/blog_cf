<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donor extends Model
{
    protected $table = 'donors';

    protected $fillable = [
        'name',
        'lastname',
        'dni',
        'phone',
        'name_company',
        'ruc'
    ];
}
