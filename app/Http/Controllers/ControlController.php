<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;

use App\Tag;
use App\Control;
use App\Donation;
use App\Article;
use App\Article_Tag;

use DB;

use Carbon\Carbon;
use Response;
use Illuminate\Support\Collection;

class ControlController extends Controller
{
    public function __construct()
    {
    		
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request){
            $query=trim($request->get('searchText'));
            $pedidos=DB::table('articles as a')->join('article_tag as rt', 'a.id', '=', 'rt.article_id')->join('tags as t', 't.id', '=', 'rt.tag_id')->join('users as u', 'u.id', '=', 'a.user_id')
            ->select('u.name', 'rt.tag_id', 't.name')
            ->where('rt.article_id', 'LIKE', '%'.$query.'%')->get();

            $donations=DB::table('donations as d')->select('name')->get();

            $tags=DB::table('tags as t')->select('t.name')->get();

            return view('admin.control.index', ["pedidos"=>$pedidos, "searchText"=>$query, "donations"=>$donations,"tags"=>$tags,]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $donations = Donation::orderBy('name', 'asc')->pluck('name', 'id');
        $tags = Tag::orderBy('name', 'asc')->pluck('name', 'id');
        return view('admin.control.create', 
        compact('donations', 'tags'));


        // dd($donation);
        // $pedidos = ArticleTag::orderBy('tag_id', 'asc')->pluck('tag_id', 'name');
        // dd($pedidos);
        //dd($article);

        // $pedidos=DB::table('articles as a')->join('article_tag as rt', 'a.id', '=', 'rt.article_id')->join('tags as t', 't.id', '=', 'rt.tag_id')
        // ->select('rt.tag_id', 't.name')->where('a.id', '=', 2)->get();
        // //dd($pedidos);
        // return view('admin.control.create', 
        // compact('pedidos'));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // $donations = Donation::orderBy('name', 'asc')->pluck('name', 'id');
        // $tags = Tag::orderBy('name', 'asc')->pluck('name', 'id');
        // if($request){
        // $query=trim($request->get('id'));
        // $dona=trim($request->get('dona'));
        // $need=trim($request->get('need'));
        // $pedidos=DB::table('articles as a')->join('article_tag as rt', 'a.id', '=', 'rt.article_id')->join('tags as t', 't.id', '=', 'rt.tag_id')->join('users as u', 'u.id', '=', 'a.user_id')->join('controls as c', 'a.id', '=', 'c.article_id')->join('donations as d', 'd.id', '=', 'c.donation_id')
        // ->select('u.name', 'rt.tag_id', 't.name')
        // ->where('rt.article_id', '=', '%'.$query.'%', $dona, '=', $need)->get();

        $control = new Control($request->all());
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
