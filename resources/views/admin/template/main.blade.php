<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <title>@yield('title') | ONIFUR</title>
</head>
<body class="bg-white">
    
    @include('admin.partials.nav')
    <div class="container">
        @include('flash::message')
    </div
    <br>
    <br>
    <section class="container bg-light col-md-8">
        <br>
        <div class="h1">
            <h1>@yield('title')</h1>
        </div>
        @include('admin.partials.errors')
        @yield('content')
        <br>
    </section>

    <script src="{{ asset('jquery/jquery-3.3.1.slim.js') }}"></script>
    <script src="{{ asset('js/app.js') }}"></script>
</body>
</html>