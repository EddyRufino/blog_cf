@extends('admin.template.main')

@section('title', 'Login')
@section('content')
    {!! Form::open(['route' => 'admin.auth.login', 'method' => 'POST']) !!}
        <div class="form-control">
            {!! Form::label('email', 'Correo Electronico') !!}
            {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Example@gmail.com']) !!}
        </div>
        <div class="form-control">
            {!! Form::label('password', 'Password') !!}
            {!! Form::email('password', ['class' => 'form-control', 'placeholder' => '******']) !!}
        </div>
        <div class="form-group">
            {!! Form::submit('Ingresar', ['class' => 'btn btn-primary']) !!}
        </div>
    {!! Form::close() !!}
@endsection