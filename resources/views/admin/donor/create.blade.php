@extends('layouts.app')

@section('title', 'Add Donante')

@section('content')
  {!! Form::open(['route' => 'donor.store', 'method' => 'POST']) !!}
    <div class="form-group">
      {!! Form::label('name', 'Nombre') !!}
      {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre', 'required' => 'true']) !!}
    </div>

    <div class="form-group">
      {!! Form::label('lastname', 'Apellidos') !!}
      {!! Form::text('lastname', null, ['class' => 'form-control', 'placeholder' => 'Nombre', 'required' => 'true']) !!}
    </div>

    <div class="form-group">
      {!! Form::submit('Agregar', ['class' => 'btn btn-primary']) !!}
    </div>
  
    {!! Form::close() !!}
@endsection