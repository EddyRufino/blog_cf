@extends('layouts.app')

@section('title', 'Tags')

@section('content')

    <a href="{{ route('tags.create') }}" class="btn btn-primary left">New Tags</a>

    <!-- Inicio del buscador -->
    {!! Form::open(['route' => 'tags.index',
        'method' => 'GET',
        'class' => 'navbar-form pull-right']) !!}

        <div class="input-group mb-3">
        {!! Form::text('name', null, [
            'class' => 'form-control' , 
            'placeholder' => 'Buscar Tags',
            'aria-describedby' => "search"]) !!}
        <span class="input-group-text poin" id="search">Buscar</span>
        </div>

    {!! Form::close() !!}
    <!-- Fin del buscador-->

    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($tags as $tag)
                <tr>
                    <td>{{ $tag->id }}</td>
                    <td>{{ $tag->name }}</td>
                    <td>
                        <a href="{{ route('tags.edit', $tag->id), $tag->id }}" class="btn btn-info">Editar</a>
                        <a href="{{ route('admin.tags.destroy', $tag->id) }}" class="btn btn-danger">Eliminar</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $tags->render() }}
@endsection