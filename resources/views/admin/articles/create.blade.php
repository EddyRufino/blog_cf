@extends('layouts.app')

@section('title', 'Add Pedido')

@section('content')
    {!! Form::open(['route' => 'articles.store', 
                    'method' => 'POST', 'files' => true]) !!}
        <div class="form-group">
            {!! Form::label('title', 'Dirección') !!}
            {!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Dirección', 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('category_id', 'Ciudad') !!}
            {!! Form::select('category_id', $categories, null, ['class' => 'form-control',
            'placeholder' => 'Select...','required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('content', 'Contenido') !!}
            {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('tags', 'Necesidades') !!}
            {!! Form::select('tags[]', $tags, null, ['class' => 'form-control',
            'multiple', 'required']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('image', 'Imagen') !!}
            <br>
            {!! Form::file('image')!!}
        </div>
        <div class="form-group">
            {!! Form::submit('Agregar', ['class' => 'btn btn-primary']) !!}
        </div>
    {!! Form::close() !!}
@endsection