@extends('layouts.app')
@section('title', 'List the users')

@section('content')

    <a href="{{ route('users.create') }}" class="btn btn-info left">Nuevo Usuario</a>
    
    <table class="table table table-striped table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Email</th>
                <th>Tipo</th>
                <th colspan="2">Acción</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($users as $user)
            <tr>
                <td>{{ $user->id }}</td>
                <td>{{ $user->name}}</td>
                <td>{{ $user->email }}</td>
                <td>
                    @if ($user->type == 'admin')
                        <span  class="btn btn-warning">{{$user->type}}</span>
                    @else
                        <span  class="btn btn-primary">{{$user->type}}</span>
                    @endif
                </td>
                <td>
                    <a href="{{ route('users.edit', $user->id) }}" class="btn btn-success">Editar</a>
                    <a href="{{ route('admin.users.destroy', $user->id) }}" class="btn btn-danger">Eliminar</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>

    {!! $users->render() !!}

@endsection