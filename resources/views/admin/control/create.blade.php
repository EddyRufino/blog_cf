@extends('layouts.app')

@section('title', 'Add Control')

@section('content')

{{-- {!! Form::open(['route' => 'control.index',
'method' => 'GET',
'class' => 'navbar-form pull-right']) !!}

<div class="input-group mb-3">
{!! Form::text('name', null, [
    'class' => 'form-control' , 
    'placeholder' => 'Buscar Pedidos',
    'aria-describedby' => "search"]) !!}
<span class="input-group-text poin" id="search">Buscar</span>
</div>

{!! Form::close() !!} --}}


  {!! Form::open(['route' => 'control.store', 
                      'method' => 'POST']) !!}

    <div class="form-group">
      {!! Form::label('donations', 'Donativos') !!}
      {!! Form::select('donations[]', $donations, 'dona', ['class' => 'form-control', 'value' => 'dona',
      'multiple', 'required']) !!}
    </div> 

    <div class="form-group">
      {!! Form::label('tags', 'Donativos') !!}
      {!! Form::select('tags[]', $tags, 'need', ['class' => 'form-control', 'value' => 'need', 'multiple', 'required']) !!}
    </div> 

    <div class="form-group">
      {!! Form::label('article_id', 'ID') !!}
      {!! Form::number('article_id', 'id', ['class' => 'form-control', 'value' => 'id', 'required']) !!}
    </div> 
    
     <div class="form-group">
      {!! Form::submit('Agregar', ['class' => 'btn btn-primary']) !!}
    </div>
  {!! Form::close() !!}
@endsection