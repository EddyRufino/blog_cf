@extends('layouts.app')

@section('title', 'Listar')

@section('content')

<a href="{{ route('control.create') }}" class="btn btn-primary left">New control</a>

  {!! Form::open(['route' => 'control.index',
  'method' => 'GET', 'role'=>'search',
  'class' => 'navbar-form pull-right']) !!}

  <div class="input-group">
    <input type="text" class="form-control" name="searchText" placeholder="Buscar..." value="{{ $searchText }}">
    <span class="input-group-btn">
      <button type="submit" class="btn btn-primary">Buscar</button>
    </span>
  </div>

  {!! Form::close() !!}

  <table class="table table-striped table-hover">
    <thead>
        <tr>
            <th>Nombre</th>
            <th>ID Tag</th>
            <th>Necesidad</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($pedidos as $pedido)
            <tr>
                <td>{{ $pedido->name }}</td>
                <td>{{ $pedido->tag_id }}</td>
                <td>{{ $pedido->name }}</td>
                
                {{-- <td>
                    <a href="{{ route('tags.edit', $tag->id), $tag->id }}" class="btn btn-info">Editar</a>
                    <a href="{{ route('admin.tags.destroy', $tag->id) }}" class="btn btn-danger">Eliminar</a>
                </td> --}}
            </tr>
        @endforeach
    </tbody>
  </table>

@endsection