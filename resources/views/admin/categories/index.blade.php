@extends('layouts.app')

@section('title', 'Categorias')

@section('content')

    <a href="{{ route('categories.create') }}" class="btn btn-primary left">New Category</a>

    <table class="table table-striped table-hover">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>Acciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($categories as $category)
                <tr>
                    <td>{{ $category->id }}</td>
                    <td>{{ $category->name }}</td>
                    <td>
                        <a href="{{ route('categories.edit', $category->id), $category->id }}" class="btn btn-info">Editar</a>
                        <a href="{{ route('admin.categories.destroy', $category->id) }}" class="btn btn-danger">Eliminar</a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{ $categories->render() }}
@endsection