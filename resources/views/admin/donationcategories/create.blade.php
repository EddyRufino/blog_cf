@extends('layouts.app')

@section('title', 'Add Categoria')

@section('content')
  {!! Form::open(['route' => 'donationcategories.store', 'method' => 'POST']) !!}
    <div class="form-group">
      {!! Form::label('name', 'Nombre') !!}
      {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre', 'required' => 'true']) !!}
    </div>

    <div class="form-group">
      {!! Form::submit('Agregar', ['class' => 'btn btn-primary']) !!}
    </div>
  
    {!! Form::close() !!}
@endsection