@extends('layouts.app')

@section('title', 'Add Donación')

@section('content')
  {!! Form::open(['route' => 'donation.store', 'method' => 'POST']) !!}
  <div class="form-group">
    {!! Form::label('name', 'Nombre') !!}
    {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Nombre Donación', 'required' => 'true']) !!}
  </div>

  <div class="form-group">
    {!! Form::label('donationcategory_id', 'Categoria') !!}
    {!! Form::select('donationcategory_id', $categories, null, ['class' => 'form-control',
    'placeholder' => 'Select...','required']) !!}
  </div>

  <div class="form-group">
    {!! Form::label('cantidad', 'Cantidad') !!}
    {!! Form::number('cantidad', null, ['class' => 'form-control', 'placeholder' => 'Cantidad Donación', 'required' => 'true']) !!}
  </div>

  <div class="form-group">
    {!! Form::label('unidad_medida', 'Unidad Medida') !!}
    {!! Form::text('unidad_medida', null, ['class' => 'form-control', 'placeholder' => 'Unidad Donación', 'required' => 'true']) !!}
  </div>

  <div class="form-group">
    {!! Form::label('donor_id', 'Donante') !!}
    {!! Form::select('donor_id', $donor, null, ['class' => 'form-control',
    'placeholder' => 'Select...','required']) !!}
  </div>

  <div class="form-group">
    {!! Form::submit('Agregar', ['class' => 'btn btn-primary']) !!}
  </div>

  {!! Form::close() !!}
@endsection